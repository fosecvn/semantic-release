FROM golang:1.19 as builder

WORKDIR /src

COPY go.mod go.sum ./
RUN go mod download

COPY . ./

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags "-s -w" -o semantic-release  ./cmd/semantic-release

FROM alpine
ARG VERSION

ADD ./docker/entrypoint.sh /usr/local/bin/docker-entrypoint
RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates
COPY --from=builder /src/semantic-release /usr/local/bin/semantic-release
RUN semantic-release --ci-condition gitlab --download-plugins --dry --provider gitlab

#ENTRYPOINT ["docker-entrypoint"]
